#ifndef LISTASDOBLES_H_INCLUDED
#define LISTASDOBLES_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "../Clientes/cliente.h"
#define Persona persona

typedef struct NodoB{
    Persona * ptCliente;
    struct NodoB * ptProximo;
    struct NodoB * ptAnterior;
} NodoB;

typedef enum { CLIENT, PRODUCTS } byType;

void inicListaDoble( NodoB ** );
void mostrarLista( NodoB * );

NodoB * insertarNodoAlPrinc( NodoB *, NodoB * );
NodoB * insertarNodoAlMedio( NodoB *, NodoB *, byType );
NodoB * insertarNodoAlFinal( NodoB *, NodoB * );

void insertarNodoTipoDeCliente( NodoB **, NodoB ** );
void insertarNodoCantidadProductos( NodoB **, NodoB ** );

NodoB * crearNodoListaDoble( Persona * );
NodoB * borrarNodoListaDoble( NodoB *, Persona * );
NodoB * vaciarLista( NodoB ** );

NodoB * buscarUltimo( NodoB * );

#endif // LISTASDOBLES_H_INCLUDED
