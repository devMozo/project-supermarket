#include "listasDobles.h"

void inicListaDoble( NodoB ** pptLista ){
    (*pptLista) = NULL;
}

void mostrarLista( NodoB * ptLista ){

    if( ptLista != NULL ){

        printf( "\n[%s + CP = %d TC = %d]->", ptLista->ptCliente->nombreApellido, ptLista->ptCliente->cantArticulos, ptLista->ptCliente->tipo_cliente );

        mostrarLista( ptLista->ptProximo );
    }
}

NodoB * insertarNodoAlPrinc( NodoB * ptLista, NodoB * ptNodo ){

    if( ptLista != NULL ){
        ptNodo->ptProximo = ptLista;
        ptLista->ptAnterior = ptNodo;
    }

    return ptNodo;
}

NodoB * insertarNodoAlMedio( NodoB * ptLista, NodoB * ptNodo, byType enumType  ){

    if( ptLista != NULL ){

        NodoB * ptSiguiente = ptLista->ptProximo;
        NodoB * ptAnterior = ptLista;

        if( enumType == CLIENT ){
             while ( ptSiguiente != NULL && ptNodo->ptCliente->tipo_cliente > ptSiguiente->ptCliente->tipo_cliente ){
                ptAnterior = ptSiguiente;
                ptSiguiente = ptSiguiente->ptProximo;
            }
        }
        else{
             while ( ptSiguiente != NULL && ptNodo->ptCliente->cantArticulos > ptSiguiente->ptCliente->cantArticulos ){
                ptAnterior = ptSiguiente;
                ptSiguiente = ptSiguiente->ptProximo;
            }
        }

        ptAnterior->ptProximo = ptNodo;
        ptNodo->ptAnterior = ptAnterior;
        ptNodo->ptProximo = ptSiguiente;

        if( ptSiguiente != NULL )
            ptSiguiente->ptAnterior = ptNodo;
    }

    return ptLista;
}

NodoB * insertarNodoAlFinal( NodoB * ptLista, NodoB * ptNodo ){

    if( ptLista != NULL ){

        NodoB * ptNodoAux = ptLista;

        while( ptNodoAux->ptProximo != NULL )
            ptNodoAux = ptNodoAux->ptProximo;

        ptNodoAux->ptProximo = ptNodo;
        ptNodo->ptAnterior = ptNodoAux;
    }
    else
        ptLista = ptNodo;

    return ptLista;
}

NodoB * insertarNodoAlFinalByPriority( NodoB * ptLista, NodoB * ptNodo ){

    if( ptLista != NULL ){

        NodoB * ptSiguiente = ptLista->ptProximo;
        NodoB * ptAnterior = ptLista;

            while ( ptSiguiente != NULL && ptNodo->ptCliente->tipo_cliente >= ptSiguiente->ptCliente->tipo_cliente ){
                ptAnterior = ptSiguiente;
                ptSiguiente = ptSiguiente->ptProximo;
            }

        ptAnterior->ptProximo = ptNodo;
        ptNodo->ptAnterior = ptAnterior;
        ptNodo->ptProximo = ptSiguiente;

        if( ptSiguiente != NULL )
            ptSiguiente->ptAnterior = ptNodo;
    }

    return ptLista;
}

void insertarNodoTipoDeCliente( NodoB ** pptLista, NodoB ** pptNodo ){

    if( (*pptLista) == NULL )
        (*pptLista) = (*pptNodo);
    else{

        NodoB * ptSiguiente = (*pptLista)->ptProximo;
        NodoB * ptAnterior = (*pptLista);

        while ( ptSiguiente != NULL && (*pptNodo)->ptCliente->tipo_cliente > ptSiguiente->ptCliente->tipo_cliente ){
                ptAnterior = ptSiguiente;
                ptSiguiente = ptSiguiente->ptProximo;
        }

        if( ptAnterior->ptAnterior == NULL && (*pptNodo)->ptCliente->tipo_cliente < ptAnterior->ptCliente->tipo_cliente ){
            (*pptLista) = insertarNodoAlPrinc( (*pptLista), (*pptNodo) );
        }
        else{
            if( ptSiguiente == NULL ){
                (*pptLista) = insertarNodoAlFinal( (*pptLista), (*pptNodo) );
            }
            else{
                (*pptLista) = insertarNodoAlMedio( (*pptLista), (*pptNodo), CLIENT );
            }
        }
    }
}

void insertarNodoCantidadProductos( NodoB ** pptLista, NodoB ** pptNodo ){

    if( (*pptLista) == NULL )
        (*pptLista) = (*pptNodo);
    else{

        NodoB * ptSiguiente = (*pptLista)->ptProximo;
        NodoB * ptAnterior = (*pptLista);

        while ( ptSiguiente != NULL && (*pptNodo)->ptCliente->cantArticulos > ptSiguiente->ptCliente->cantArticulos ){
                ptAnterior = ptSiguiente;
                ptSiguiente = ptSiguiente->ptProximo;
        }

        if( ptAnterior->ptAnterior == NULL && (*pptNodo)->ptCliente->cantArticulos < ptAnterior->ptCliente->cantArticulos ){
            (*pptLista) = insertarNodoAlPrinc( (*pptLista), (*pptNodo) );
        }
        else{
            if( ptSiguiente == NULL ){
                (*pptLista) = insertarNodoAlFinal( (*pptLista), (*pptNodo) );
            }
            else{
                (*pptLista) = insertarNodoAlMedio( (*pptLista), (*pptNodo), PRODUCTS );
            }
        }
    }
}

NodoB * crearNodoListaDoble( Persona * ptNewCliente ){

    NodoB * ptNodo = calloc( 1, sizeof( NodoB ) );

    ptNodo->ptAnterior = NULL;
    ptNodo->ptProximo = NULL;
    ptNodo->ptCliente = ptNewCliente;

    return ptNodo;
}

NodoB * borrarNodoListaDoble( NodoB * ptLista, Persona * ptCliente ){

    NodoB * ptAux = ptLista;
    bool bReady = true;

    while( ptAux != NULL && bReady ){

        if( strcmp( ptAux->ptCliente->nombreApellido, ptCliente->nombreApellido ) == 0 ){

            if( ptAux->ptAnterior != NULL ){
                ptAux->ptAnterior->ptProximo = ptAux->ptProximo;
            }
            if( ptAux->ptProximo != NULL ){
                ptAux->ptProximo->ptAnterior = ptAux->ptAnterior;
            }

            ptAux = NULL;
            free( ptAux );
            bReady = false;
        }
        else
            ptAux = ptAux->ptProximo;
    }

    return ptLista;
}

NodoB * buscarUltimo( NodoB * ptLista ){

    if( ptLista != NULL )
        while( ptLista->ptProximo != NULL )
            ptLista = ptLista->ptProximo;

    return ptLista;
}

