#ifndef _GENERADORH_
#define _GENERADORH_
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <string.h>

typedef struct
{
    short int vocals;              /// Un maximo de DOS vocales

    short int consonants;          /// Un maximo de TRES consonantes s�lo si la primera fue Sufija y la segunda intermedia.
    /// Caso contrario el m�ximo es DOS.

    short int wasUsed;             /// Contador interno que determina la cantidad de veces que una letra es usada continuamente.
    //-0 : no.
    //-1.
    //-2 : Deber� obligar a que una nueva y diferente letra sea elegida y se reiniciar� el contador.
    short int Continuas;           /// Un maximo de DOS seguidas.


} Cantidades;


typedef struct
{

    short int wasEnd;                     /// Si es la ultima letra. Se coloca un espacio para separarla del apellido.
    //-0 : no is a end letter.
    //-1 : colocar� un espacio.

    short int wasvocal;                 ///A, E, I, O, U.
    short int wasconsonant;               ///Q, W, R, T, Y, P, S, D, F, G, H, J, K, L, Z, X, C, V, B, N, M.
    short int wasintermediate;          /// Si puede ir antes de otra consonante, De acuerdo a la letra se definir� la siguiente.
    short int wascontinua;                /// Si es una R o una T o una S o una H o una K.
    short int wasSufija;                /// Si despu�s de una vocal la letra es una R, S, X, N, M, D, G.

    short int wasprevioustovocal;       /// Si despues de ella debe ir si o si una vocal.
    short int wasprevioustoconsonant;     /// Si en el caso de ir dos vocales seguidas, o, sea necesario, seguir� una consonante.

    char letra;                         /// Debe de representar a una letra ya usada.

} Letra;

///GENERA NUMEROS ALEATORIOS ENTRE INICIO Y LIMITE INCLUSIVE.
int AleatNumber(int inicio, int Limite);
float AleatFloat(int inicio, int limite);

///GENERA LETRAS ALEATORIAS EN MINUSCULA;
char AleatLetterMin(void);

///GENERA NOMBRES DE FORMA ALEATORIA DE HASTA 1, 2 o 3 NOMBRES.
void AleatName(char Nombre[], int Dim);

//Inicializa los tipos de letra.
void inicLetras(Letra * LETT );
//Inicializa las cantidades.
void inicCantidades(Cantidades * CANTS);
//CAMBIA MINUSCULAS POR MAYUSCULAS.
char toUpper(char minuscula);

//REVISA LETRAS PARA DETERMINAR SU TIPO.
int letra_Is(char tipoDe_Ltra[], char letra , short int DimTipo_Letra);

//FUNCIONES DE COLOCACION DE LETRAS.
void Primera_letra(Letra * actuals, char vocals[], char consonants[], short int DimV, short int DimC);

#endif
