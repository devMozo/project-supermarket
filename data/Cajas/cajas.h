#ifndef CAJAS_H_INCLUDED
#define CAJAS_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include "../Filas/filas.h"
#include "../Clientes/cliente.h"
#include "../Generadores/generador.h"
#define Persona persona
#define maxCajas 8
#define TIME_OF_QUANTUM 2

typedef enum { SRTF, PNA, FIFO, RR } algorithmType;

typedef struct Algorithm{

    char * cpAlgoritmoPlanificacion; /// Debe informar que tipo de alg. de planificaci�n utiliza la caja
    int iTiempoTotal; /// Tiempo total del algoritmo
    algorithmType eAlgType; /// Tipo de algoritmo

    Fila tFilaProcessedClient; /// Clientes ya procesados

    /**
        Array paralelos:
            - Un array de enteros que especifica en que tiempo debe entrar
              otro cliente.
            - Un array de personas, donde especifica todos los datos del cliente.

            Si a la hora de entrar el Cliente, la caja esta llena, quedara en el arreglo
            como si fuera un SWAP
    */

    int sizeArrays; /// Dimension

    int * iAddInTime;
    Persona * ptAddInTimePeople;
    byType * peType;

} Algorithm;

typedef struct Caja{
     bool bAbierta;

     int iNro_de_caja;
     int iTipo_pago; // 1 efectivo, 2 cr�dito o d�bito, 3 todos

     char * cpNombreCajero;
     Algorithm algoritmo;

     Fila tFila;
} Caja;


void inicArregloCajas( Caja ** );
void crearCaja( Caja *, bool );
void agregarCaja( Caja **, Caja * );
void abrirOcerrarCaja( Caja **, int );
Caja buscarCaja( Caja *, int, int );
void mostrarUnaCaja( Caja );
void mostrarCaja( Caja * );
void agregarClienteACaja( Caja **, Persona * );
void agregarClienteACajaEnTiempoDeterminado( Caja **, Persona *, int, byType );
void atenderClientes( Caja *, algorithmType );

/** Algoritmos de planificacion y sus callbacks **/

void planningAlgorithm( Caja *, void (*callbackExec)( Caja *, NodoB **, int * ), void (*callbackSort)( NodoB ** ) );

void algorithmSRTF( Caja *, NodoB **, int * );
void sortSRTF( NodoB ** );

void algorithmPNA( Caja *, NodoB **, int * );
void sortPNA( NodoB ** );

void algorithmFIFO( Caja *, NodoB **, int * );
void sortFIFO( NodoB ** );

void algorithmRR( Caja *, NodoB **, int * );
void sortRR( NodoB ** );

/// Re ordenar clientes

NodoB * reOrderClientsByPriority( NodoB ** ); /// Por Prioridad
NodoB * reOrderClientsByQuantity( NodoB ** ); /// Por Cantidad

/// Verificar si hay clientes para entrar en la caja

bool verifyAndLoadClient( int, Caja **, bool ); /// En tiempo de ejecucion
bool isThereAnotherClient( Caja ); /// Cuando la lista quede en NULL

/// Ver los resultados de cada cliente

void seeTheResultList( Caja * );


#endif // CAJAS_H_INCLUDED
