#include "cajas.h"

void inicArregloCajas( Caja ** pptCaja ){

    int i = 0;

    (*pptCaja) = calloc( 8, sizeof( Caja ) );

    for( i = 0; i < maxCajas; i++ ){
        (*pptCaja)[i].iNro_de_caja = -1;
        initFila( &(*pptCaja)[i].tFila );
    }
}

/// Creo una caja, puede crearse automaticamente o a manopla.

void crearCaja( Caja * ptNewCaja, bool bManual ){

    /// Inicializo los arreglos de char.

    ptNewCaja->algoritmo.cpAlgoritmoPlanificacion = malloc( 30 );
    ptNewCaja->cpNombreCajero = malloc( 30 );

    /// Si es manual, voy pidiendo todos los datos al usuario, y que los vaya ingresando.

    if( bManual ){
        puts( "Ingrese Tipo de Pago: " );
        puts( "Ingrese: 1 - Efectivo. 2 - Credito o Debito. 3 - Todos " );
        scanf( "%d", &ptNewCaja->iTipo_pago );

        puts( "Como se llama el cajero? " );
        fflush( stdin );
        fgets( ptNewCaja->cpNombreCajero, 30, stdin );

        puts( "Que algoritmo desea usar? " );
        puts( "Ingrese: SRTF - SRTF (SJF apropiativo. PNA - Prioridades no apropiativo. FIFO - First In First Out. RR - Round Robin. (Default - FIFO)" );
        fflush( stdin );
        fgets( ptNewCaja->algoritmo.cpAlgoritmoPlanificacion, 30, stdin );
    }

    /// Si es automatico, se genera todo y lo grabo instantaneamente en la Caja
    else{
        int iRandomNumber = ( rand()%3 )+1;

        ptNewCaja->iTipo_pago = iRandomNumber;
        AleatName( ptNewCaja->cpNombreCajero, 3 );

        iRandomNumber = rand()%4;

        switch( iRandomNumber ){
            case 0:
                strcpy( ptNewCaja->algoritmo.cpAlgoritmoPlanificacion , "SRTF" );
                ptNewCaja->algoritmo.eAlgType = SRTF;
            break;
            case 1:
                strcpy( ptNewCaja->algoritmo.cpAlgoritmoPlanificacion , "PNA" );
                ptNewCaja->algoritmo.eAlgType = PNA;
            break;
            case 2:
                strcpy( ptNewCaja->algoritmo.cpAlgoritmoPlanificacion , "FIFO" );
                ptNewCaja->algoritmo.eAlgType = FIFO;
            break;
            case 3:
                strcpy( ptNewCaja->algoritmo.cpAlgoritmoPlanificacion , "RR" );
                ptNewCaja->algoritmo.eAlgType = RR;
            break;
            default:
                strcpy( ptNewCaja->algoritmo.cpAlgoritmoPlanificacion , "RR" );
                ptNewCaja->algoritmo.eAlgType = RR;
            break;
        }
    }

    ptNewCaja->algoritmo.iTiempoTotal = 0;
    ptNewCaja->algoritmo.sizeArrays = 0;

    ptNewCaja->bAbierta = true;

    ptNewCaja->iNro_de_caja = -1;
    initFila( &ptNewCaja->algoritmo.tFilaProcessedClient );
    initFila( &ptNewCaja->tFila );
}

/// Paso 2 parametros, el puntero al array de cajas y el puntero de la nueva caja

void agregarCaja( Caja ** pptCaja, Caja * ptNewCaja ){

    int i = 0;

    /// Busco la primer caja vacia

    while( (*pptCaja)[i].iNro_de_caja != -1 && i < maxCajas )
        i++;

    /// Valido que no este lleno el arreglo.

    if( (*pptCaja)[i].iNro_de_caja != -1 && i == maxCajas-1 )
        puts( "Ya no entran mas cajas." );
    else{
        ptNewCaja->iNro_de_caja = i;
        (*pptCaja)[i] = *ptNewCaja;
    }
}

/// Paso dos parametros, un puntero al array de Cajas, y la posicion de la caja que se desea abrir o cerrar

void abrirOcerrarCaja( Caja ** pptCaja, int pos ){

    /// Si la posicion es mayor a 7 aviso que se paso.

    if( pos < maxCajas && pptCaja[pos] != NULL ){

        /// Hago un toggle respecto al atributo bAbierta.

        if( (*pptCaja)[pos].bAbierta == false )
            (*pptCaja)[pos].bAbierta = true;
        else
            (*pptCaja)[pos].bAbierta = false;
    }
    else
        puts( "Se paso del rango de cajas o directamente no existe." );
}

/// Paso 3 parametros, uno de la estructura caja, otro del tipo de pago, y un index que indique
/// a partir de que posicion buscar.

Caja buscarCaja( Caja * ptCaja, int iTipoDePago, int iStart ){

    int i = iStart;

    /// Si devuelvo una caja con Nro. de Caja -1, quiere decir que no encontro una caja.

    Caja tReturned;
         tReturned.iNro_de_caja = -1;

    /// Busco la caja validando si es igual al tipo de pago;

    while( ptCaja[i].iTipo_pago != iTipoDePago && i < maxCajas )
        i++;

    if( ptCaja[i].iTipo_pago == iTipoDePago )
        tReturned = ptCaja[i];

    /// Retorno la caja

    return tReturned;
}

void mostrarCaja( Caja * ptCaja ){

    int i = 0;

    for( i = 0; i < maxCajas; i++ ){

        printf( "/----------- Caja %d -----------------/\n", i );

        mostrarUnaCaja( ptCaja[i] );
    }
}

void mostrarUnaCaja( Caja ptCaja ){

    if( ptCaja.iNro_de_caja == -1 )
        puts( "Ta vachia :(" );
    else{
        if( ptCaja.bAbierta == true )
            printf( "\nAbierta\n" );
        else
            printf( "\nCerrada\n" );

        printf( "Numero de Caja: %d \n", ptCaja.iNro_de_caja );

        switch( ptCaja.iTipo_pago ){
            case 1:
                puts( "Tipo de Pago: Efectivo" );
            break;
            case 2:
                puts( "Tipo de Pago: Credito O Debito" );
            break;
            case 3:
                puts( "Tipo de Pago: Todo" );
            break;
            default:
                puts( "Tipo de Pago: No se puede mostrar, error." );
            break;
        }

        printf( "Nombre del cajero: %s\n", ptCaja.cpNombreCajero );
        printf( "Algoritmo de Planificacion: %s\n", ptCaja.algoritmo.cpAlgoritmoPlanificacion );

        showFila( ptCaja.tFila );
        puts( "" );
    }
}

void agregarClienteACaja( Caja ** pptCaja, Persona * ptCliente ){

    int i = 0,
        finalPos = -1;

    if( ptCliente != NULL ){

        while( i < maxCajas ){
            if( (*pptCaja)[i].bAbierta && ( (*pptCaja)[i].iTipo_pago == ptCliente->tipo_pago || (*pptCaja)[i].iTipo_pago == 3 ) ){
                if( finalPos == -1 )
                    finalPos = i;
                else
                    if( getSize( (*pptCaja)[i].tFila ) < getSize( (*pptCaja)[finalPos].tFila ) )
                        finalPos = i;
            }
            i++;
        }

        ptCliente->tiempoDeEntrada = 0;
        ptCliente->timeOfExecution = ptCliente->cantArticulos;

        if( finalPos != -1 ){
            if( (*pptCaja)[finalPos].algoritmo.eAlgType == FIFO || (*pptCaja)[finalPos].algoritmo.eAlgType == SRTF )
                addToFila( &(*pptCaja)[finalPos].tFila, ptCliente, PRODUCTS );
            else
                addToFila( &(*pptCaja)[finalPos].tFila, ptCliente, CLIENT );
        }
        else
            printf( "No se pudo encontrar una caja para %s. \n", ptCliente->nombreApellido );
    }
}

void agregarClienteACajaEnTiempoDeterminado( Caja ** pptCaja, Persona * ptCliente, int iTimeToGo, byType eType ){

    int i = (*pptCaja)->algoritmo.sizeArrays;

    if( i == 0 ){
        (*pptCaja)->algoritmo.iAddInTime = calloc( i+1, sizeof( int ) );
        (*pptCaja)->algoritmo.peType = calloc( i+1, sizeof( byType ) );
        (*pptCaja)->algoritmo.ptAddInTimePeople = calloc( i+1, sizeof( Persona ) );
    }
    else{
        (*pptCaja)->algoritmo.iAddInTime = realloc( (*pptCaja)->algoritmo.iAddInTime, (i+1)*sizeof( int ) );
        (*pptCaja)->algoritmo.peType = realloc( (*pptCaja)->algoritmo.peType, (i+1)*sizeof( byType ) );
        (*pptCaja)->algoritmo.ptAddInTimePeople = realloc( (*pptCaja)->algoritmo.ptAddInTimePeople, (i+1)*sizeof( Persona ) );
    }

    ptCliente->tiempoDeEntrada = iTimeToGo;
    ptCliente->timeOfExecution = ptCliente->cantArticulos;

    (*pptCaja)->algoritmo.iAddInTime[i] = iTimeToGo;
    (*pptCaja)->algoritmo.peType[i] = eType;
    (*pptCaja)->algoritmo.ptAddInTimePeople[i] = *ptCliente;
    (*pptCaja)->algoritmo.sizeArrays++;

}

void atenderClientes( Caja * ptCaja, algorithmType eType ){

    if( ptCaja->bAbierta == true ){
        switch( eType ){
            case SRTF:
                planningAlgorithm( ptCaja, algorithmSRTF, sortSRTF );
            break;
            case PNA:
                planningAlgorithm( ptCaja, algorithmPNA, sortPNA );
            break;
            case FIFO:
                planningAlgorithm( ptCaja, algorithmFIFO, sortFIFO );
            break;
            case RR:
                planningAlgorithm( ptCaja, algorithmRR, sortRR );
            break;
            default:
                planningAlgorithm( ptCaja, algorithmFIFO, sortFIFO );
            break;
        }
    }
    else
        puts( "Esta cerrada macho, no podemos hacer nada, es el fin." );
}

void planningAlgorithm( Caja * ptCaja, void (*callbackExec)( Caja *, NodoB **, int * ), void (*callbackSort)( NodoB ** ) ){

    int iTimer = 0;

    NodoB * ptNodeinExcution = NULL;

    inicListaDoble( &ptNodeinExcution );

    Persona * newCliente = inicCliente();

    if( ptCaja->tFila.ptInit != NULL )
        ptNodeinExcution = ptCaja->tFila.ptInit;

    while( ptNodeinExcution != NULL ){

        /// Ejecucion

        callbackExec( ptCaja, &ptNodeinExcution, &iTimer );

        /// Tomar los tiempos de retorno, espera y procesado.

        if( ptNodeinExcution->ptCliente->cantArticulos  == 0 ){
            ptNodeinExcution->ptCliente->tiempoDeRetorno = iTimer - ptNodeinExcution->ptCliente->tiempoDeEntrada;
            ptNodeinExcution->ptCliente->tiempoProcesado = iTimer;
            ptNodeinExcution->ptCliente->tiempoDeEspera = ptNodeinExcution->ptCliente->tiempoDeRetorno - ptNodeinExcution->ptCliente->timeOfExecution;

            newCliente = ptNodeinExcution->ptCliente;

            addToFila( &ptCaja->algoritmo.tFilaProcessedClient, newCliente, PRODUCTS );
        }

        /// Reorden de los Clientes.

        callbackSort( &ptNodeinExcution );

        /// Chequear si hay algun otro Cliente que vaya a entrar mas tarde.

        if( ptNodeinExcution == NULL && isThereAnotherClient( *ptCaja )  ){
            while( ptNodeinExcution == NULL ){
                verifyAndLoadClient( iTimer, &ptCaja, true );
                ptNodeinExcution = ptCaja->tFila.ptInit;
                iTimer++;
            }
        }
    }
}

void algorithmSRTF( Caja * ptCaja, NodoB ** pptNode, int * piTimer ){

    while( (*pptNode)->ptCliente->cantArticulos > 0 ){

            ptCaja->tFila.ptInit = (*pptNode);
            verifyAndLoadClient( *piTimer, &ptCaja, true );
            (*pptNode) = ptCaja->tFila.ptInit;

            *piTimer = *piTimer + 1;
            (*pptNode)->ptCliente->cantArticulos--;

            system( "pause" );
            system( "cls" );
            printf( "Tiempo: %ds\n\n", *piTimer );

            mostrarUnaCaja( *ptCaja );
            puts( "" );
    }
}

void sortSRTF( NodoB ** pptNodo ){

    while( (*pptNodo) != NULL && (*pptNodo)->ptCliente->cantArticulos == 0 )
        (*pptNodo) = (*pptNodo)->ptProximo;

}

void algorithmPNA( Caja * ptCaja, NodoB ** pptNode, int * piTimer ){

    while( (*pptNode)->ptCliente->cantArticulos > 0 ){

        ptCaja->tFila.ptInit = (*pptNode);
        verifyAndLoadClient( *piTimer, &ptCaja, false );
        (*pptNode) = ptCaja->tFila.ptInit;

        *piTimer = *piTimer + 1;
        (*pptNode)->ptCliente->cantArticulos--;

        system( "pause" );
        system( "cls" );
        printf( "Tiempo: %ds\n\n", *piTimer );

        mostrarUnaCaja( *ptCaja );
        puts( "" );
    }
}

void sortPNA( NodoB ** pptNodo ){

    (*pptNodo) = reOrderClientsByPriority( pptNodo );

    while( (*pptNodo) != NULL && (*pptNodo)->ptCliente->cantArticulos == 0 )
        (*pptNodo) = (*pptNodo)->ptProximo;

}

void algorithmFIFO( Caja * ptCaja, NodoB ** pptNode, int * piTimer ){

    while( (*pptNode)->ptCliente->cantArticulos > 0 ){

            ptCaja->tFila.ptInit = (*pptNode);
            verifyAndLoadClient( *piTimer, &ptCaja, false );
            (*pptNode) = ptCaja->tFila.ptInit;

            *piTimer = *piTimer + 1;
            (*pptNode)->ptCliente->cantArticulos--;

            system( "pause" );
            system( "cls" );
            printf( "Tiempo: %is\n\n", *piTimer );

            mostrarUnaCaja( *ptCaja );
            puts( "\n" );
    }
}

void sortFIFO( NodoB ** pptNodo ){

    (*pptNodo) = reOrderClientsByQuantity( pptNodo );
}

void algorithmRR( Caja * ptCaja, NodoB ** pptNode, int * piTimer ){

    int iQuantum = 0;

    while( (*pptNode)->ptCliente->cantArticulos > 0 && iQuantum < TIME_OF_QUANTUM ){

        ptCaja->tFila.ptInit = (*pptNode);
        verifyAndLoadClient( *piTimer, &ptCaja, true );
        (*pptNode) = ptCaja->tFila.ptInit;

        *piTimer = *piTimer + 1;
        iQuantum++;
        (*pptNode)->ptCliente->cantArticulos--;

        system( "pause" );
        system( "cls" );
        printf( "Tiempo: %ds\n\n", *piTimer );

        mostrarUnaCaja( *ptCaja );
        puts( "" );
    }
}

void sortRR( NodoB ** pptNodo ){

    NodoB * ptAux = NULL,
          * ptAux2;

    if( (*pptNodo)->ptProximo != NULL ){
        if( (*pptNodo)->ptCliente->cantArticulos != 0 ){

            ptAux = crearNodoListaDoble( (*pptNodo)->ptProximo->ptCliente );
            ptAux2 = crearNodoListaDoble( (*pptNodo)->ptCliente );

            (*pptNodo) = borrarNodoListaDoble( (*pptNodo), (*pptNodo)->ptProximo->ptCliente );
            (*pptNodo) = (*pptNodo)->ptProximo;

            (*pptNodo) = insertarNodoAlPrinc( (*pptNodo), ptAux );
            insertarNodoAlFinalByPriority( (*pptNodo), ptAux2 );
        }
        else
            (*pptNodo) = reOrderClientsByPriority( pptNodo );
    }
    else{
        if( (*pptNodo)->ptCliente->cantArticulos == 0 )
            (*pptNodo) = NULL;
    }
}

NodoB * reOrderClientsByPriority( NodoB ** pptNodo ){

    NodoB * ptNodoLast = buscarUltimo( (*pptNodo) ),
          * ptNodoAux = NULL,
          * unitNodo = NULL;

    while( ptNodoLast->ptAnterior != NULL ){

        unitNodo = crearNodoListaDoble( ptNodoLast->ptCliente );

        insertarNodoTipoDeCliente( &ptNodoAux, &unitNodo );
        ptNodoLast = ptNodoLast->ptAnterior;
    }

    return ptNodoAux;
}

NodoB * reOrderClientsByQuantity( NodoB ** pptNodo ){

    NodoB * ptNodoLast = buscarUltimo( (*pptNodo) ),
          * ptNodoAux = NULL,
          * unitNodo = NULL;

    while( ptNodoLast->ptAnterior != NULL ){

        unitNodo = crearNodoListaDoble( ptNodoLast->ptCliente );

        insertarNodoCantidadProductos( &ptNodoAux, &unitNodo );
        ptNodoLast = ptNodoLast->ptAnterior;
    }

    return ptNodoAux;
}

/// 1) Verificar si hay un nuevo elemento para agregar y si lo hay agregarlo.
/// Tambien analizar la posibilidad de si es Apropiativo o No Apropiativo
/// 2) Si es apropiativo dezplaza al que este en ejecucion.
/// 3) El que sale de ejecucion se va a la cola de Listos.

bool verifyAndLoadClient( int iTimer, Caja ** pptCaja, bool isApropiative ){

    int i = 0,
        tipoPagoCliente = 0;

    bool bResponse = false;

    while( i < (*pptCaja)->algoritmo.sizeArrays ){

        tipoPagoCliente = (*pptCaja)->algoritmo.ptAddInTimePeople[i].tipo_pago;

        if( (*pptCaja)->algoritmo.iAddInTime[i] <= iTimer &&
           ( tipoPagoCliente == 3 || (*pptCaja)->iTipo_pago == 3 || tipoPagoCliente == (*pptCaja)->iTipo_pago )){

            if( isApropiative != true )
                addToFilaToTheEnd( &(*pptCaja)->tFila , &(*pptCaja)->algoritmo.ptAddInTimePeople[i] );
            else
                addToFila( &(*pptCaja)->tFila , &(*pptCaja)->algoritmo.ptAddInTimePeople[i], (*pptCaja)->algoritmo.peType[i] );

            (*pptCaja)->algoritmo.sizeArrays--;
            bResponse = true;
        }
        i++;
    }

    return bResponse;
}

bool isThereAnotherClient( Caja tCaja ){

    bool bResponse = false;
    int i = 0;

    int pagoCaja = tCaja.iTipo_pago,
        pagoCliente = 0;

    for( i = 0; i < tCaja.algoritmo.sizeArrays; i++ ){
        pagoCliente = tCaja.algoritmo.ptAddInTimePeople[i].tipo_pago;

        if( pagoCaja == 3 || pagoCliente == 3 || pagoCaja == pagoCliente )
            bResponse = true;
    }

    return bResponse;
}

void seeTheResultList( Caja * ptCaja ){

    int i = 0;

    NodoB * ptAux;

    for( i = 0; i < maxCajas; i++ ){

        ptAux = ptCaja[i].algoritmo.tFilaProcessedClient.ptInit;

        while( ptAux != NULL ){
            printf( "\nNombre: %s, Tiempo de Retorno: %d, Tiempo de Espera: %d, Tiempo de procesado %d \n", ptAux->ptCliente->nombreApellido, ptAux->ptCliente->tiempoDeRetorno, ptAux->ptCliente->tiempoDeEspera, ptAux->ptCliente->tiempoProcesado );
            ptAux = ptAux->ptProximo;
        }
    }
}










