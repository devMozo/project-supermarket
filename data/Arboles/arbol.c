#include "arbol.h"

nodoArbol * inicArbol()
{
    return NULL;
}
nodoArbol * crearNodoArbol(persona cliente)
{
    nodoArbol * nuevoNodo = (nodoArbol*) malloc(sizeof(nodoArbol));
    nuevoNodo->p=cliente;
    nuevoNodo->izq=NULL;
    nuevoNodo->der=NULL;
    return nuevoNodo;
}
///Pasar del archivo al arbol
nodoArbol * archivoToArbol(nodoArbol * arbol,char *archivo)
{
    FILE *parch=fopen(archivo,"rb");
    persona aux;
    nodoArbol * nuevo=NULL;
    if(parch)
    {
        while(fread(&aux,1,sizeof(persona),parch)>0)
        {
            nuevo=crearNodoArbol(aux);
            arbol=insertarOrdenadoNombre(arbol, nuevo);
        }
    }
    return arbol;
}
///Insertar en orden por nombre
nodoArbol * insertarOrdenadoNombre(nodoArbol * arbol,nodoArbol * nuevoNodo)
{
    if(arbol==NULL)
    {
        arbol=nuevoNodo;
    }
    else
    {
        if(strcmp(arbol->p.nombreApellido,nuevoNodo->p.nombreApellido)<0)
            arbol->der = insertarOrdenadoNombre(arbol->der, nuevoNodo);
        else
            arbol->izq = insertarOrdenadoNombre(arbol->izq, nuevoNodo);
    }
    return arbol;
}
///PnOrder
void inOrder(nodoArbol * arbol)
{
    if(arbol != NULL)
    {
    inOrder(arbol->izq);
    printf("\n----------------------------");
    printf("\n %s",arbol->p.nombreApellido);
    printf("\n----------------------------");
    inOrder(arbol->der);
    }
}
///PreOrder
void preOrder(nodoArbol * arbol)
{
    if(arbol != NULL)
    {
    printf("\n----------------------------");
    printf("\n %s",arbol->p.nombreApellido);
    printf("\n----------------------------");
    preOrder(arbol->izq);
    preOrder(arbol->der);
    }
}
///PostOrder
void postOrder(nodoArbol * arbol)
{
    if(arbol != NULL)
    {
    postOrder(arbol->izq);
    postOrder(arbol->der);
    printf("\n----------------------------");
    printf("\n %s",arbol->p.nombreApellido);
    printf("\n----------------------------");
    }
}

///Funcion Mostrar
int DibujarArbolLinea(struct nodoArbol * arbol, int esIzq, int inicia, int nivel, char s[20][255])
{
    char b[10];
    int ancho = 4; //es el ancho del nodo que dibujo
    int i;

    if (!arbol) return 0;

    sprintf(b, "[%d", arbol->p.cantArticulos); // agrego valor de nodo a la matriz

    int izq  = DibujarArbolLinea(arbol->izq,  1, inicia,                nivel + 1, s);
    int der = DibujarArbolLinea(arbol->der, 0, inicia + izq + ancho, nivel + 1, s);

    //dibujo nodo
    for (i = 0; i < ancho; i++)
        s[2 * nivel][inicia + izq + i] = b[i];

    //dibujo lineas .----+----.
    if (nivel && esIzq)
    {

        for (i = 0; i < ancho + der; i++)
            s[2 * nivel - 1][inicia + izq + ancho/2 + i] = '-';

        s[2 * nivel - 1][inicia + izq + ancho/2] = '.';
        s[2 * nivel - 1][inicia + izq + ancho + der + ancho/2] = '+';

    }
    else if (nivel && !esIzq)
    {

        for (i = 0; i < izq + ancho; i++)
            s[2 * nivel - 1][inicia - ancho/2 + i] = '-';

        s[2 * nivel - 1][inicia + izq + ancho/2] = '.';
        s[2 * nivel - 1][inicia - ancho/2 - 1] = '+';
    }

    //Cerebros quemados everywere!!
    return izq + ancho + der;
}

int DibujarArbol(struct nodoArbol * arbol)
{
    char s[20][255];
    int i;
    //creo una matriz de char para hacer mi "dibujo"
    for (i = 0; i < 20; i++)
        sprintf(s[i], "%80s", " ");

    //Hago magia
    DibujarArbolLinea(arbol, 0, 0, 0, s);

    //Imprimo matris
    for (i = 0; i < 10; i++)
        printf("%s\n", s[i]);
    return 0;
}
///Borrar un nodo por nombre
nodoArbol * borrarPorNombre(nodoArbol * arbol, char *nombre)
{
    if(arbol!=NULL)
    {
        if(strcmp(arbol->p.nombreApellido,nombre)<0)
        {
            arbol->der=borrarPorNombre(arbol->der, nombre);
        }
        else if (strcmp(arbol->p.nombreApellido,nombre)>0)
        {
            arbol->izq=borrarPorNombre(arbol->izq, nombre);
        }
        else
        {
            if(arbol->izq!=NULL)
            {
                arbol->p=nodoMasDerecho(arbol->izq);
                arbol->izq=borrarPorNombre(arbol->izq, arbol->p.nombreApellido);
            }
            else if(arbol->der!=NULL)
            {
                arbol->p=nodoMasIzquierdo(arbol->der);
                arbol->der=borrarPorNombre(arbol->der, arbol->p.nombreApellido);
            }
            else
            {
                free(arbol);
                arbol=NULL;
            }
        }
    }
    return arbol;
}
///Nodo mas derecho
persona nodoMasDerecho(nodoArbol * arbol)
{
    persona aux;
    if(arbol->der==NULL)
    {
        aux=arbol->p;
    }
    else
    {
        aux=nodoMasDerecho(arbol->der);
    }
    return aux;
}
///Nodo mas izquierdo
persona nodoMasIzquierdo(nodoArbol * arbol)
{
    persona aux;
    if(arbol->izq==NULL)
    {
        aux=arbol->p;
    }
    else
    {
        aux=nodoMasIzquierdo(arbol->izq);
    }
    return aux;
}

 /// Funcion de pasar de arbol to Caja.
 void arbolAcaja( nodoArbol * arbol, Caja ** cajaActual )
 {
     if( arbol != NULL ){

        arbolAcaja( arbol->izq, cajaActual );
        arbolAcaja( arbol->der, cajaActual );
        agregarClienteACaja( &(*cajaActual), &arbol->p );

     }

 }

