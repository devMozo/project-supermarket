#ifndef _ARBOLH_
#define _ARBOLH_
#include "../Clientes/cliente.h"
#include "../Cajas/cajas.h"

typedef struct nodoArbol
{
    persona p;
    struct nodoArbol * izq;
    struct nodoArbol * der;

} nodoArbol;

///Mostrar Arbol
int DibujarArbol( struct nodoArbol * arbol );
int DibujarArbolLinea( struct nodoArbol * arbol, int esIzq, int inicia, int nivel, char s[20][255] );
///InOrder
void inOrder( nodoArbol * arbol );
///PreOrder
void preOrder( nodoArbol * arbol );

nodoArbol * inicArbol(  );
nodoArbol * crearNodoArbol( persona cliente );
nodoArbol * cargarNodoArbol(  );
nodoArbol * insertarOrdenadoNombre( nodoArbol * arbol, nodoArbol * nuevoNodo );
nodoArbol * borrarPorNombre( nodoArbol * arbol, char * nombre );
nodoArbol * archivoToArbol( nodoArbol * arbol, char * archivo );
persona nodoMasIzquierdo( nodoArbol * arbol );
persona nodoMasDerecho( nodoArbol * arbol );
void arbolAcaja( nodoArbol * arbol, Caja ** cajaActual );

#endif


