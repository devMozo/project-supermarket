#ifndef FILAS_H_INCLUDED
#define FILAS_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "../Clientes/cliente.h"
#include "../ListasDobles/listasDobles.h"
#define Persona persona

typedef struct Fila{
    NodoB * ptInit;
    NodoB * ptEnd;
}Fila;

void initFila( Fila * );
void addToFila( Fila *, Persona *, byType );
void addToFilaToTheEnd( Fila *, Persona * );
void deleteToFila( Fila *, Persona * );
int getSize( Fila );
void showFila( Fila );
bool isEmpty( Fila );

#endif // FILAS_H_INCLUDED
