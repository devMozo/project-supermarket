#include "filas.h"

void initFila( Fila * ptFila ){
    inicListaDoble( &ptFila->ptEnd );
    inicListaDoble( &ptFila->ptInit );
}

void addToFila( Fila * ptFila, Persona * tCliente, byType eType ){

    NodoB * ptNewCliente = crearNodoListaDoble( tCliente );

    if( eType == CLIENT ){
        insertarNodoTipoDeCliente( &ptFila->ptInit, &ptNewCliente );
        ptFila->ptEnd = buscarUltimo( ptFila->ptInit );
    }
    else{
        insertarNodoCantidadProductos( &ptFila->ptInit, &ptNewCliente );
        ptFila->ptEnd = buscarUltimo( ptFila->ptInit );
    }
}

void addToFilaToTheEnd( Fila * ptFila, Persona * tCliente ){

    NodoB * ptNewCliente = crearNodoListaDoble( tCliente );

    ptFila->ptInit = insertarNodoAlFinal( ptFila->ptInit, ptNewCliente );
    ptFila->ptEnd = ptNewCliente;

}

void deleteToFila( Fila * ptFila, Persona * tCliente ){
    ptFila->ptInit = borrarNodoListaDoble( ptFila->ptInit, tCliente );
    ptFila->ptEnd = buscarUltimo( ptFila->ptInit );
}

int getSize( Fila filita ){

    int i = 0;

    NodoB * ptAux = filita.ptInit;

    while( ptAux != NULL ){
        i++;
        ptAux = ptAux->ptProximo;
    }

    return i;
}

void showFila( Fila tFila ){
    mostrarLista( tFila.ptInit );
}

bool isEmpty( Fila tFila ){

    bool bResponse = false;

    if( tFila.ptInit != NULL && tFila.ptEnd != NULL )
        bResponse = true;

    return bResponse;
}
