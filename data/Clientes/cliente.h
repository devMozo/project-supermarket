#ifndef _CLIENTEH_
#define _CLIENTEH_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../Generadores/generador.h"

typedef struct persona{
    char nombreApellido [40];
    int tiempoDeEntrada;
    int timeOfExecution; /// Esta es como Cant de articulos pero constante, se usa como referencia.
    int tiempoDeRetorno;
    int cantArticulos; /// es el tiempo de ejecuci�n
    int tiempoDeEspera; /// es el tiempo de respuesta
    int tiempoProcesado; /// es el tiempo que ya fue procesado en la l�nea de caja
    int tipo_cliente; /// prioridad (1: embarazada, 2: jubilado y 3: normal)
    int tipo_pago; /// 1 efectivo, 2 cr�dito o d�bito, 3 todos
} persona;

void crearArchivoClientes(char *archivo);
void cargarArchivoClientes(char *archivo, int);
void mostrarArchivoClientes(char *archivo);
persona * inicCliente();
persona * crearCliente();

#endif
