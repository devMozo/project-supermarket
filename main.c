#include <stdio.h>
#include <stdlib.h>
#include "data/Clientes/cliente.h"
#include "data/Filas/filas.h"
#include "data/Generadores/generador.h"
#include "data/Arboles/arbol.h"
#include "data/Cajas/cajas.h"
#include <unistd.h>
#define Persona persona

void inicRocket( void );

const char rocket[] =
"           _\n\
          /^\\\n\
          |-|\n\
          | |\n\
          |N|\n\
          |A|\n\
          |S|\n\
          |A|\n\
         /| |\\\n\
        / | | \\\n\
       |  | |  |\n\
        `-\"\"\"-`\n\
";


int main()
{
    srand(time(NULL));
    configuracion();
    inicRocket();
    nodoArbol * arbol;
    char * archivo = (char*)malloc(sizeof(char)*30);
    int i = 0,
        iOption = 0,
        clientNumber = 0;


    Caja * cajitas;
    Caja nuevaCaja;

    while( iOption != -1 ){

        system( "cls" );
        puts( "Programa: Mischuk and Mozo.\n\n\n\t 0 - Crear Archivo. \n\t 1 - Pasar los clientes al archivo. \n\t 2 - Inicializar Arbol. \n\t 3 - Mostrar Arbol. \n\t 4 - Inicializar Cajas. \n\t 5 - Pasar de Arbol a Cajas. \n\t 6 - Agregar Cliente en caja en tiempo determinado. \n\t 7 - Mostrar cajas. \n\t 8 - Atender Clientes. \n\t 9 - See the result list. \n\t -1 - Exit." );
        scanf( "%d", &iOption );

        switch( iOption ){
            case 0:
                system( "cls" );
                /// Crear Archivo

                strcpy( archivo, "input.dat" );
                crearArchivoClientes( archivo );
                system( "pause" );
            break;
            case 1:
                system( "cls" );
                /// Pasar los clientes al archivo

                puts( "Cuantos clientes desea cargar?" );
                scanf( "%d", &clientNumber );

                cargarArchivoClientes( archivo, clientNumber );
                mostrarArchivoClientes( archivo );
                system( "pause" );
            break;
            case 2:
                system( "cls" );
                ///Inicializar Arbol

                arbol = inicArbol(  );
                arbol = archivoToArbol( arbol, archivo );
                system( "pause" );
            break;
            case 3:
                system( "cls" );
                /// Mostrar Arbol

                inOrder( arbol );
                system( "pause" );
            break;
            case 4:
                system( "cls" );
                /// Inicializar Cajas

                inicArregloCajas( &cajitas );

                for( i = 0; i < maxCajas; i++ ){
                    crearCaja( &nuevaCaja, false );
                    agregarCaja( &cajitas, &nuevaCaja );
                }
                system( "pause" );
            break;
            case 5:
                system( "cls" );
                /// Pasar de Arbol a Cajas

                arbolAcaja( arbol, &cajitas );
                system( "pause" );
            break;
            case 6:
                system( "cls" );

                /// Agregar Cliente en caja en tiempo determinado.

                Persona * ptClienteEspecial = calloc( 1, sizeof( Persona ) );

                printf( "\nIngresar Nombre y Apellido:" );
                fflush( stdin );
                fgets( ptClienteEspecial->nombreApellido, 30, stdin );

                puts( "Cantidad de Articulos:" );
                scanf( "%d", &ptClienteEspecial->cantArticulos );

                puts( "Tipo de Cliente: 1 - Embarazada. 2 - Jubilado. 3 - Otro." );
                scanf( "%d", &ptClienteEspecial->tipo_cliente );

                puts( "Tipo de pago: 1 - Efectivo. 2 - Credito o Debito. 3 - Todos." );
                scanf( "%d", &ptClienteEspecial->tipo_pago );

                int timeToGo = 0;

                puts( "En que momento quiere entrar?" );
                scanf( "%d", &timeToGo );

                agregarClienteACajaEnTiempoDeterminado( &cajitas, ptClienteEspecial, timeToGo, PRODUCTS );

                system( "pause" );
            break;
            case 7:
                system( "cls" );

                /// Mostrar cajas.

                mostrarCaja( cajitas );
                system( "pause" );
            break;
            case 8:
                system( "cls" );
                /// Atender Clientes

                for( i = 0; i < maxCajas; i++ ){
                    atenderClientes( &cajitas[i], cajitas[i].algoritmo.eAlgType );
                }
                system( "pause" );
            break;
            case 9:
                system( "cls" );
                /// See the result list

                seeTheResultList( cajitas );
                system( "pause" );
            break;
        }
    }


    return 0;
}

void inicRocket(){

    int i = 0;

    for ( i = 0; i < 50; i ++){
        printf("\n"); // jump to bottom of console
    }
        printf("%s", rocket);


    int j = 300000;
    for (i = 0; i < 50; i ++) {
        usleep(j); // move faster and faster,
        j = (int)(j * 0.9); // so sleep less each time
        printf("\n"); // move rocket a line upward
    }
}
